#!/bin/sh
#
# Copyright (c) 2022 Sean Davies <skd@skd.id.au>
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE

PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

maillog=/var/log/maillog
maxfailures=10
pftable=smtpd_failures

die()
{
	echo "$1" 1>&2
	exit 1
}

usage()
{
	die "usage: ${0##*/} [-d | -m max] [-f file]"
}

verifyarg()
{
	local _arg="$1" _num

	case ${_arg} in
	''|*[!0-9]*) die "${0##*/}: ${_arg}: invalid number" ;;
	esac

	_num=$(echo "${_arg}" | sed 's/^0*//')
	if [ "${#_num}" -gt 3 ] || [ -z "${_num}" ]; then
		die "${0##*/}: ${_num}: out of range"
	fi
	echo "${_num}"
}

smtpdauthfailures()
{
	local _sessionid _sessionids

	_sessionids=$(awk '/smtp authentication.*result=permfail/ \
		{ print $6 }' "${maillog}")

	for _sessionid in ${_sessionids}; do
		[ "${#_sessionid}" -eq 16 ] || continue
		grep "${_sessionid}.*address=" "${maillog}" | \
			sed 's/\(^.*=\)\(.*\)\(\ .*$\)/\2/'
	done | sort | uniq -c
}

smtpduserfailures()
{
	local _failedusers

	_failedusers=$(awk '/smtp authentication.*result=permfail/ \
		{ print substr($9, 6) }' "${maillog}" | sort | uniq -c)

	if [ -z "${_failedusers}" ]; then
		_failedusers="no user information to display"
	fi
	echo "${_failedusers}"
}

dflag=0
mflag=0
while getopts df:m: arg; do
	case ${arg} in
	d)	dflag=1 ;;
	f)	maillog=${OPTARG} ;;
	m)	maxfailures=$(verifyarg "${OPTARG}") || exit 1
		mflag=1 ;;
	*)	usage ;;
        esac
done
shift $((OPTIND - 1))
[ "$#" -eq 0 ] || usage

if [ "${dflag}" -eq 1 ] && [ "${mflag}" -eq 1 ]; then
	usage
fi

if [ "${dflag}" -eq 0 ] && [ "$(id -u)" -ne 0 ]; then
	die "${0##*/}: needs root privileges"
fi

if [ ! -f "${maillog}" ]; then
	die "${0##*/}: ${maillog}: no such file"
fi

authfailures=$(smtpdauthfailures)
[ -n "${authfailures}" ] || exit 0

if [ "${dflag}" -eq 0 ]; then
	pftableaddrs=$(pfctl -t "${pftable}" -T show 2>/dev/null)

	echo "${authfailures}" | while read -r authfailure; do
		failures=$(echo "${authfailure}" | cut -d ' ' -f 1)

		if [ "${failures}" -ge "${maxfailures}" ]; then
			addr=$(echo "${authfailure}" | cut -d ' ' -f 2)

			if ! echo "${pftableaddrs}" | grep -q "${addr}"; then
				pfctl -t "${pftable}" -T add "${addr}" 2>/dev/null
				echo "${0##*/}: ${addr} added"
			fi
		fi
	done
else
	if [ -n "${authfailures}" ]; then
		printf "%s\n\n" "${authfailures}"
		smtpduserfailures
	fi
fi
