SMTPD-FAILURES(8) - System Manager's Manual

# NAME

**smtpd-failures** - add failed smtpd authentication attempts to a pf table

# SYNOPSIS

**smtpd-failures**
\[**-d**&nbsp;|&nbsp;**-m**&nbsp;*max*]
\[**-m**&nbsp;*file*]

# DESCRIPTION

**smtpd-failures**
is a utility that will add failed
smtpd(8)
authentication attempts to a
pf(4)
table.

When run without any options the default
smtpd(8)
log file will be checked for failed authentication attempts and once there has
been more than 10 from the same IP address, the IP address will be added to a
pf(4)
table called
*smtpd\_failures*.

Alternatively, the total count of each IP address and username that failed
authentication can be printed to stdout.

The options are as follows:

**-d**

> Debug mode.
> Print failed authentication information and exit.

**-f** *file*

> Use an alternative maillog file.

**-m** *max*

> Set maximum authentication failures to add to the
> pf(4)
> table.

# FILES

*/var/log/maillog*

> Default
> smtpd(8)
> maillog file.

# EXIT STATUS

The **smtpd-failures** utility exits&#160;0 on success, and&#160;&gt;0 if an error occurs.

# EXAMPLES

The following lines when added to
pf.conf(5)
create an empty
pf(4)
table and define a filter rule to block all traffic
coming from addresses listed in the
pf(4)
table.

	table <smtpd_failures> persist
	block drop quick on egress from <smtpd_failures> to any

**smtpd-failures**
is then run from
crontab(5)
every 5 minutes to add addresses which have reached the failed authentication
limit of 5 to the
pf(4)
table.

	*/5 * * * * /usr/local/bin/smtpd-failures -m 5

# SEE ALSO

pf(4),
crontab(5),
pf.conf(5),
cron(8),
pfctl(8),
smtpd(8)

# AUTHORS

Sean Davies &lt;[skd@skd.id.au](mailto:skd@skd.id.au)&gt;

# CAVEATS

**smtpd-failures**
only takes into account the number of failed authentications within the log
file, not when the log file is rotated or time between authenticaiton attempts.
Additionally the IP addresses added to the
pf(4)
table are not persistent across
reboots.
